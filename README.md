# README #

1/3スケールのSHARP X-1c風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- シャープ

## 発売時期
- 1983年10月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/X1_(%E3%82%B3%E3%83%B3%E3%83%94%E3%83%A5%E3%83%BC%E3%82%BF))
- [「僧兵ちまちま」のゲーム日記。](https://blog.goo.ne.jp/timatima-psu/e/646fdd3c38b9c78cff281b361691db13)
- [懐かしのパソコン](https://greendeepforest.com/?p=2401)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_x1c/raw/6b533138e30739ded5f72d4428cfd795fa3e59bd/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x1c/raw/6b533138e30739ded5f72d4428cfd795fa3e59bd/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x1c/raw/6b533138e30739ded5f72d4428cfd795fa3e59bd/ExampleImage.jpg)
